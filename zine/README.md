# Z I N E

This here is the heart of your song Island.  It's called zine, because this island is really just a dat zine with a musical focus, and I like the smooth consistency of consistency. To learn more about dat zines, check out [The origin story for the dat zine library](https://coolguy.website/projects/dat-zine-library/index.html#origin-story).  In short, these are meant to stand as their own beautiful object, but also have built-in tools so distributing it digitally among yr friends is easier.  

This folder contains the mp3, the image, and the details for your island.  All of this can be edited from within yr song island by clicking the edit pencil in the top right.  You can also directly edit the files from within beaker if you'd like, with the only cauting being that the site assumes there'll be a `songIsland.zine` structured in a specific way, and so you can edit the contents, but it's recommended that you keep the file and it's structure.

Heck yah have at it!
