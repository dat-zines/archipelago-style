import { default as cssToJson } from 'css2json';
import { writable, readable, derived, get } from 'svelte/store';
import { parse } from 'smarkt';

import * as archipelagoData from '../../zine/song-islands.json';

async function fetchIsland (island ) {
  // grabs an island's url and builds up a full object from it,
  // by requesting the right files from the island's dat.
  try {
    let archive = new DatArchive(island.url);
    let songIsland = await archive
        .readFile('zine/song-island.zine')
        .then(contents => parse(contents));
    let distro = await archive
        .readFile('distro/info.zine')
        .then(contents => parse(contents));
    let aesthetic = await archive
        .readFile('aesthetic/custom.css')
        .then(contents => generateAesthetic(contents));
    let url = island.url.replace(/\/$/,'');
    return {...songIsland, distro, aesthetic, island: url};
  } catch (err) {
    return {...island, err}
  }
}


const fetchAllIslands = async (set, archipelago, missing) => {
  // used to set a writable store.  Will iterate over the islands in our archipelago.
  // parsing their zine and distro files, buiilding a store from the returned values.
  // if missing is set, we instead build a store from all the failed attempts at parsing.
  let songIslands = archipelago;
  let tl = await Promise.all(songIslands.map(fetchIsland));
  set(tl.filter(island => !island.hasOwnProperty('err')));
};

export const library = new DatArchive(window.location); 
export const tracklist = writable([], (set) => fetchAllIslands(set, archipelagoData.default))

export const hiddenTracks = writable([], (set) => async() => {
  let songIslands = archipelagoData.default;
  let tl = await Promise.all(songIslands.map(fetchIsland))
  set(tl.filter(island => island.hasOwnProperty('err')));
});

export const islandCount = derived(tracklist, $tracklist => tracklist.length)

export const currentTrack = writable(0);

export const currentSong = derived([tracklist, currentTrack], ([$tracklist, $currentTrack], set) => {
  if ($tracklist.length === 0) {
    set({})
  } else {
    set($tracklist[$currentTrack]);
  }
});

export const nextSong = derived([tracklist, currentTrack], ([$tl, $ct], set) => {
  let nextIndex = ($ct === $tl.length - 1) ? 0 : $ct + 1;
  console.log({nextIndex})
  set($tl[nextIndex])
});

export const newestSong = derived(tracklist, ($tl) => $tl[$tl.length - 1]);

export const previousSongs = writable([])
export const previousSong = derived(previousSongs, ($previousSongs, set) => {
  if ($previousSongs.length > 0) {
    set($previousSongs[0])
  } else {
    set({})
  }
})

export const audio = writable({})
export const currentTime = writable(0);
export const duration = writable(0);
export const ct = derived(currentTime, $a => formatTime($a));
export const dt = derived(duration, $a => formatTime($a));
export const paused = writable(true);
export const press= derived(audio, $a => {  
  // the buttons you can press.  lil pun for you ;) .  it's important to have fun.
  return {
    play: ()=> $a.play(),
    pause: () => $a.pause(),
    forwardSeek: (secs) => ($a.currentTime += secs),
    reverseSeek: (secs) => ($a.currentTime -= secs),
    nextTrack: () => switchToNextTrack(),
    prevTrack: () => switchToPreviousTrack(),
    playThisTrack: (track) => switchToThisTrack(track)
  }
})

function formatTime(seconds) {
  // given time in seconds, format aesthetically pleasing minute'seconds display
  if (isNaN(seconds)) return '...';

  const minutes = Math.floor(seconds / 60);
  seconds = Math.floor(seconds % 60);
  if (seconds < 10) seconds = '0' + seconds;

  return `${minutes}:${seconds}`;
};

const switchToNextTrack = () => {
  let cs = get(currentSong)
  let tl = get(tracklist)
  currentTrack.update((ct) => {
    if (ct === tl.length - 1) {
      return 0;
    } else {
      return ct + 1;
    }
  })
  previousSongs.update((ps) => [cs, ...ps])
}

const switchToPreviousTrack = () => {
  let ps = get(previousSong);
  let tl = get(tracklist);
  let cs = get(currentSong)
  currentTrack.update((ct) => tl.findIndex(track => track.island === ps.island));
  previousSongs.update((p) => [cs, ...p])
};

const switchToThisTrack = (t) => {
  let tl = get(tracklist);
  let cs = get(currentSong);
  previousSongs.update(ps => [cs, ...ps]);
  currentTrack.update((ct) => tl.findIndex(track => track.island === t.island));
};

export const addNewIsland = async(island) => {
  let newArchipelago = [...archipelagoData.default, {...island}]
  library.writeFile('zine/song-islands.json', JSON.stringify(newArchipelago, null, 2))
  let newIsland = await fetchIsland(island);
  tracklist.update(tl => [...tl, newIsland]);
};

export const removeIsland = async(islandIndex) => {
  let newArchipelago = archipelagoData.default.filter((i, idx) => idx !== islandIndex)
  library.writeFile('zine/song-islands.json', JSON.stringify(newArchipelago, null, 2))
  tracklist.update(tl => tl.filter((i, idx) => idx !== islandIndex))
}

let varToCamel = (str) => {
  // given a css variable string, return it without dashes and camel-cased
  return str
    .replace('--','')
    .split('-')
    .map((word, idx) => idx === 0 ? word : word[0].toUpperCase() + word.slice(1))
    .join('')
}

function generateAesthetic (css) {
  let cssVarData = cssToJson(css)[":root"]
  if (cssVarData["--theme"]) {
    cssVarData["--theme"] = cssVarData["--theme"].replace(/"/g,"");
  }
  let cssVarObj = Object.keys(cssVarData).reduce((o, key) => ({...o, [varToCamel(key)]: cssVarData[key]}),{})
  let cssVarStr = Object.keys(cssVarData).reduce((acc, key) => (acc.concat(`${key}: ${cssVarData[key]}; `)),"");
  return {...cssVarObj, style: cssVarStr}
}

function absolutePath (path, url) {
  // this assumes they uploaded image through our editor, and it's held in one path above.  not sure how to check all relative paths and repalce them properly.
  let validSchemes = ['https://', 'dat://']
  let pathIsAbsolute = validSchemes.some(scheme => path.startsWith(scheme))
  if (pathIsAbsolute) {
    return path
  } else {
    path = path
      .replace('url(..', '')
      .replace(/\)$/,'')
    return `url(${url}${path})`
  }
};

