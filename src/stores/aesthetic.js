import { default as cssToJson } from 'css2json';
import { get, writable, derived } from 'svelte/store';
import { currentSong } from './archipelago.js'

let varToCamel = (str) => {
  // given a css variable string, return it without dashes and camel-cased
  return str
    .replace('--','')
    .split('-')
    .map((word, idx) => idx === 0 ? word : word[0].toUpperCase() + word.slice(1))
    .join('')
}

export const aesthetic = derived(currentSong, ($cs, set) => {
  if ($cs.island == null) {
    set({theme: 'cats'})
  } else {
    let island = new DatArchive($cs.island) 
    island.readFile('aesthetic/custom.css').then(css => {
      let cssVarData = cssToJson(css)[":root"]
      cssVarData["--primary-bg"] = absolutePath(cssVarData["--primary-bg"], $cs.island)
      if (cssVarData["--theme"]) {
        cssVarData["--theme"] = cssVarData["--theme"].replace(/"/g,"");
      }
      // cssVarData["theme"] = cssVarData["theme"].replace(/"/g,'');
      let cssVarObj = Object.keys(cssVarData).reduce((o, key) => ({...o, [varToCamel(key)]: cssVarData[key]}),{})
      let cssVarStr = Object.keys(cssVarData).reduce((acc, key) => (acc.concat(`${key}: ${cssVarData[key]}; `)),"");
      set({...cssVarObj, style: cssVarStr})
    })
  }
})

function absolutePath (path, url) {
  // this assumes they uploaded image through our editor, and it's held in one path above.  not sure how to check all relative paths and repalce them properly.
  let validSchemes = ['https://', 'dat://']
  let pathIsAbsolute = validSchemes.some(scheme => path.startsWith(scheme))
  if (pathIsAbsolute) {
    return path
  } else {
    path = path
      .replace('url(..', '')
      .replace(/\)$/,'')
    return `url(${url}${path})`
  }
}
