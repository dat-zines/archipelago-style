# A E S T H E T I C

This be the folder for all the styling of yr zine.  CSS is incredible, and you can change the look and feel of your zine using _nothing but_ CSS.  This is a language that continually gives back, so I definitely recommend diving into this as much as you can!

The best an easiest way to customize the CSS is within the `custom.css` file.  This is generated automatically when you edit the songIsland through its own edit page (by clicking the edit pencil in the top right), but you can directly adjust this file in beaker too.  It's a set of key terms and some value, like
```
--primary-color: #fe3dc0;
```
so you can just adjust those style values and it will adjust the whole site.

If you wanna go further, and write your own css, you can do so in the main.css folder.  This is good if you want to make an aesthetic change to all elements of a type...like making all <a> tags bright green and a size 40 font(I don't know why you'd do it, but you can)!

One thing you may wanna do is change the font.  You can do that in the `main.css` file and even import in your own font.  There's a commented example of how to do this in that CSS file.


## Resources
Mozilla Dev Network has a good guide to css:
https://developer.mozilla.org/en-US/docs/Learn/CSS/Introduction_to_CSS

Another great resource is css-tricks:
https://css-tricks.com/guides/beginner/

Both of these are forever resources, like you'll find new and valuable things as you grow as a code zinester.  If there's stuff that seems intimidating or strange within the guides, it's just cos it's strange _right now_.  Future you will be all about it.
